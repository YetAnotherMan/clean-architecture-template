package com.cladeapps.todo.model

/**
 * Created by yetanotherman on 22.04.18.
 */
data class Todo(val description: String)