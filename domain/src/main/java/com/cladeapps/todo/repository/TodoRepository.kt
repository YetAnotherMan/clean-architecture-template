package com.cladeapps.todo.repository

import com.cladeapps.todo.model.Todo

/**
 * Created by yetanotherman on 22.04.18.
 */
interface TodoRepository {
    fun getTodos(): List<Todo>
}