package com.cladeapps.cleanarchitecturetemplate.todo.ui.activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.cladeapps.cleanarchitecturetemplate.R

class TodoActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
